
import { Pion, Matrix, IPosition, IMove, INextMove, IposCaptAndAsp, Type } from "src/@type/_type";
import { ROW, COLOUMN } from "src/Constante/constante";
import { fanorona } from "./fanorona";
import { Inject, Injectable } from "@nestjs/common";
@Injectable()
export class PlayerService  extends fanorona{
    pion: Pion;
    nom: Type;
    constructor() {
        super()
    }
    getBoard(){
        return this.board;
    }
    GetNumberOfOpponent(board:Matrix<string>,pion:Pion):number{
        let opponent = 0;
        for (let row = 0; row < ROW; row++) {
            for (let col = 0; col < COLOUMN; col++) {
                const current = board[row][col]
                if(current!=pion && current!=Pion.EMPTY){
                    opponent+=1
                }
            }
        }
        return opponent;
    }
    GetBestMoveForCurrentPion(board:Matrix<string>,moves:IMove[],pion:Pion):INextMove{
        let bestMove:IMove={to:{x:0,y:0},from:{x:0,y:0}};
        let bestScore = -Infinity;
        for (const move of moves) {
            let newBoard =  board.map((e)=>e.slice());
            const step = this.canCapture(newBoard, move,pion);
            newBoard = this.makeMove(newBoard,move, step,pion,false);
            const {score} = this.MinMax(newBoard,1,-Infinity,Infinity,false);
            console.log(score);
            if (score > bestScore) {
                bestScore = score;
                bestMove = move;
            }
        }
        return {bestMove,bestScore}
    }

    MoveAI(board:Matrix<string>, pion:Pion):Promise<IMove[]> {
        console.table(board)
        var listBestMove:IMove[]=[];
        return new Promise((resolve, reject) => {
            let bestScore = -Infinity;
            let bestMove = { to: { x: 0, y: 0 }, from: { x: 0, y: 0 } };
            let step:IposCaptAndAsp[]= [{ type: "aspire", value: { x: 0, y: 0 } }];
            for (let row = 0; row < ROW; row++) {
                for (let col = 0; col < COLOUMN; col++) {
                    if (board[row][col] === pion) {
                        const currentPlayer = { x: row, y: col };
                        const moves = this.generateMoves(board, currentPlayer, pion);
                        if(moves.length){
                            bestMove = this.GetBestMoveForCurrentPion(board, moves,pion).bestMove;
                        }
                    }
                }
            }
            let numberOfOpponent = this.GetNumberOfOpponent(board,pion);
            step = this.canCapture(board, bestMove, pion);
            board = this.makeMove(board, bestMove, step, pion, false);
            listBestMove.push(bestMove)
            let NewNumberOfOpponent = this.GetNumberOfOpponent(board,pion);
            while (NewNumberOfOpponent !== numberOfOpponent) {
                let currentPlayer = bestMove.to;
                bestScore = -Infinity;
                let moves = this.generateMoves(board, currentPlayer, pion);
                moves = moves.filter((move, _) => {
                    return listBestMove.findIndex((path, _) => move.to.x == path.to.x && move.to.y == path.to.y) == -1;
                })
                console.log(moves)
                bestMove = this.GetBestMoveForCurrentPion(board, moves,pion).bestMove;
                listBestMove.push(bestMove)
                step = this.canCapture(board, bestMove, pion);
                board = this.makeMove(board, bestMove, step, pion, false);
                numberOfOpponent = NewNumberOfOpponent;
                NewNumberOfOpponent = this.GetNumberOfOpponent(board,pion);
            }
            resolve(listBestMove);
        });
    }
    
}