import { Injectable } from '@nestjs/common';
import { Matrix, Pion, Vector } from 'src/@type/_type';
import { PlayerService } from './player';
import { TaquinService } from './taquin';

@Injectable()
export class AlgoService {
  constructor(
    private PlayerService : PlayerService,
    private TaquinService : TaquinService
    ){}
  MoveAI(board:Matrix<string>,pion:Pion){
    return this.PlayerService.MoveAI(board,pion)
  }
  TaquinAI(board:Vector<number>){
    return this.TaquinService.BFS(board)
  }
}
