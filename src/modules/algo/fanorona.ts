import { Matrix, IPosition, IMove, Pion, IposCaptAndAsp, IBFS } from "src/@type/_type";
import { ROW, COLOUMN, DIRECTION, DIRECTIONKELY } from "src/Constante/constante";

export class fanorona {

    board: Matrix<string> = [];
    constructor() {
        this.initialeState();
    }
    private initialeState() {
        // for (let row = 0; row < ROW; row++) {
        //     if (row < Math.floor(ROW / 2)) {
        //         this.board.push(Array(COLOUMN).fill(Pion.BLACK))
        //     } else if (row == Math.floor(ROW / 2)) {
        //         const vect: Vector<string> = Array(COLOUMN).fill(Pion.EMPTY).map(
        //             (val, index) => index != 4 ? index % 2 ? Pion.BLACK : Pion.WHITE : Pion.EMPTY
        //         )
        //         this.board.push(vect);
        //     } else {
        //         this.board.push(Array(COLOUMN).fill(Pion.WHITE))
        //     }
        // }
        this.board = [
            ['0', '0', '0', '2', '0', '0', '0', '0', '0'],
            ['1', '0', '2', '2', '2', '2', '2', '0', '0'],
            ['0', '0', '2', '2', '0', '0', '2', '0', '2'],
            ['0', '2', '0', '1', '0', '0', '0', '2', '0'],
            ['0', '1', '0', '2', '2', '2', '1', '0', '2']
        ]
    };
    private isPossibleMove(pos: IPosition): boolean {
        return (pos.x >= 0 && pos.x < ROW) && (pos.y >= 0 && pos.y < COLOUMN)
    };
    public canCapture(board: Matrix<string>, move_: IMove, player: Pion): IposCaptAndAsp[] {

        const newGrid = board.map((e) => e.slice());
        const { from, to } = move_;
        const [dx, dy] = [to.x - from.x, to.y - from.y];
        const listPosToCapture: IposCaptAndAsp[] = [];
        // Vérification de la capture en avant
        const nextPos = { x: to.x + dx, y: to.y + dy };
        if (this.isPossibleMove(nextPos) && newGrid[nextPos.x][nextPos.y] !== player && newGrid[nextPos.x][nextPos.y] !== Pion.EMPTY) {
            listPosToCapture.push({
                type: "capture",
                value: { x: dx, y: dy },
            });
        }

        // Vérification de la capture en arrière
        const nextPos1 = { x: from.x - dx, y: from.y - dy };
        if (this.isPossibleMove(nextPos1) && newGrid[nextPos1.x][nextPos1.y] !== player && newGrid[nextPos1.x][nextPos1.y] !== Pion.EMPTY) {
            listPosToCapture.push({
                type: "aspire",
                value: { x: -dx, y: -dy }
            });
        }

        return listPosToCapture;
    };

    public makeMove(grid: Matrix<string>, move_: IMove, nextStep: IposCaptAndAsp[], player: Pion, isPlayer = false): Matrix<string> {
        const newGrid = grid.map((g) => g.slice());
        const { from, to } = move_;
        newGrid[from.x][from.y] = Pion.EMPTY;
        newGrid[to.x][to.y] = player;

        if (isPlayer) {
            // player
        } else {
            if (nextStep.length > 0) {
                if (nextStep.length == 2) {
                    // const idx = Math.round(Math.random())
                    let { type, value } = nextStep[0]
                    if (type == "aspire") {
                        let captureDirection = value;
                        // On suppose que la première direction est celle de la capture
                        let capturePos = { x: from.x + captureDirection.x, y: from.y + captureDirection.y };
                        // console.log(newGrid[capturePos.x][capturePos.y])
                        while (this.isPossibleMove(capturePos) && newGrid[capturePos.x][capturePos.y] !== player && newGrid[capturePos.x][capturePos.y] !== Pion.EMPTY) {
                            newGrid[capturePos.x][capturePos.y] = Pion.EMPTY;
                            capturePos = { x: capturePos.x + captureDirection.x, y: capturePos.y + captureDirection.y };
                        }

                    } else {
                        const captureDirection = value;
                        let capturePos = { x: to.x + captureDirection.x, y: to.y + captureDirection.y };
                        while (this.isPossibleMove(capturePos) && newGrid[capturePos.x][capturePos.y] !== player && newGrid[capturePos.x][capturePos.y] !== Pion.EMPTY) {
                            newGrid[capturePos.x][capturePos.y] = Pion.EMPTY;
                            capturePos = { x: capturePos.x + captureDirection.x, y: capturePos.y + captureDirection.y };
                        }
                    }
                } else {
                    let { type, value } = nextStep[0]
                    if (type == "aspire") {
                        let captureDirection = value;
                        // On suppose que la première direction est celle de la capture
                        let capturePos = { x: from.x + captureDirection.x, y: from.y + captureDirection.y };
                        // console.log(newGrid[capturePos.x][capturePos.y])
                        while (this.isPossibleMove(capturePos) && newGrid[capturePos.x][capturePos.y] !== player && newGrid[capturePos.x][capturePos.y] !== Pion.EMPTY) {
                            newGrid[capturePos.x][capturePos.y] = Pion.EMPTY;
                            capturePos = { x: capturePos.x + captureDirection.x, y: capturePos.y + captureDirection.y };
                        }

                    } else {
                        const captureDirection = value;
                        let capturePos = { x: to.x + captureDirection.x, y: to.y + captureDirection.y };
                        while (this.isPossibleMove(capturePos) && newGrid[capturePos.x][capturePos.y] !== player && newGrid[capturePos.x][capturePos.y] !== Pion.EMPTY) {
                            newGrid[capturePos.x][capturePos.y] = Pion.EMPTY;
                            capturePos = { x: capturePos.x + captureDirection.x, y: capturePos.y + captureDirection.y };
                        }
                    }
                }
            }
        }
        return newGrid;
    };


    public generateMoves(grid: Matrix<string>, pos: IPosition, player: Pion): IMove[] {
        let moves: IMove[] = [];
        const DIR = (pos.x + pos.y) % 2 == 0 ? DIRECTION : DIRECTIONKELY;
        for (const { x, y } of DIR) {
            const newPos: IPosition = {
                x: x + pos.x,
                y: y + pos.y
            };
            if (this.isPossibleMove(newPos)) {
                const piece = grid[newPos.x][newPos.y];
                if (piece == Pion.EMPTY) {
                    moves.push({
                        from: { x: pos.x, y: pos.y },
                        to: { x: newPos.x, y: newPos.y }
                    });
                }
            }
        }

        return moves;
    };
    public findBestCapture(board: Matrix<string>, pos: IPosition, player: Pion): { bestPast: IPosition[], allPath: IBFS[] } {
        let visited = new Set();
        visited.add(JSON.stringify(pos));
        let queue: IBFS[] = [{ noeud: pos, path: [pos] }]
        let paths: IPosition[] = [];
        let bestScore = Infinity;
        let bestPath: IPosition[] = []
        let allPath: IBFS[] = []
        while (queue.length > 0) {
            const front = queue.shift() as IBFS;
            let score = 0;
            paths = front.path;
            let noeud = front?.noeud;
            const neighbors = this.generateMoves(board, noeud, player);
            let newBoard = board.map(re => re.slice());
            allPath.push(front);
            for (const neighbor of neighbors) {
                const isCanCapture = this.canCapture(board, neighbor, player);
                if (!visited.has(JSON.stringify(neighbor.to))) {
                    if (isCanCapture.length) {
                        newBoard = this.makeMove(board, neighbor, isCanCapture, player);
                        queue.push(
                            {
                                noeud: neighbor.to,
                                path: [...paths, neighbor.to]
                            }
                        )
                        visited.add(JSON.stringify(neighbor.to));
                    }
                }
            }
            let newBoardScore = board.map(e => e.slice());
            for (const path of paths) {
                const move: IMove = {
                    to: path,
                    from: noeud
                }
                const step_ = this.canCapture(newBoardScore, move, player);
                newBoardScore = this.makeMove(newBoardScore, move, step_, player);
                noeud = path;
            }
            score = this.evaluate(newBoardScore,2).score;
            if (score <= bestScore) {
                bestScore = score;
                bestPath = front.path;
            }
        }
        return { bestPast: bestPath, allPath: allPath };
    }
    public isGameOver(board: Matrix<string>, player: Pion) {
        let score = 0;
        for (let row = 0; row < ROW; row++) {
            for (let col = 0; col < COLOUMN; col++) {
                if (board[row][col] === player) {
                    score += 1;
                }
            }
        }
        return score;
    }
    public MinMax(board: Matrix<string>, depth: number, alpha: number, beta: number, isMaximizingPlayer: boolean): { score: number } {
        let bestScore = isMaximizingPlayer ? -Infinity : Infinity;
        let bestMove: IMove | null = null;
        const player = isMaximizingPlayer ? Pion.WHITE : Pion.BLACK;
        if (depth === 0 || this.isGameOver(board, player) == 0) {
            // Utilisez une fonction d'évaluation plus sophistiquée ici
            const evaluation = this.evaluate(board,depth+1);
            return evaluation;
        }

        for (let row = 0; row < ROW; row++) {
            for (let col = 0; col < COLOUMN; col++) {
                if (board[row][col] === player) {
                    const currentPlayer: IPosition = { x: row, y: col };
                    const moves = this.generateMoves(board, currentPlayer, player);
                    for (const move of moves) {
                        let newBoard = board.map(e => e.slice());
                        // newBoard = this.makeMultipleCapture(newBoard,move,currentPlayer,player);
                        let currentPos = currentPlayer;
                        const best = this.findBestCapture(newBoard, move.from, player);
                        if (best.bestPast.length > 1) {
                            for (const path of best.bestPast) {
                                const move: IMove = {
                                    to: path,
                                    from: currentPos
                                };
                                const step_ = this.canCapture(newBoard, move, player);
                                newBoard = this.makeMove(newBoard, move, step_, player);
                                currentPos = path;
                            }
                        } else {
                            newBoard[move.from.x][move.from.y] = Pion.EMPTY;
                            newBoard[move.to.x][move.to.y] = player;
                        }
                        
                        let score = isMaximizingPlayer
                            ? this.MinMax(newBoard, depth - 1, alpha, beta, false).score
                            : this.MinMax(newBoard, depth - 1, alpha, beta, true).score;
                        if ((isMaximizingPlayer && score > bestScore) || (!isMaximizingPlayer && score < bestScore)) {
                            bestScore = score;
                        }
                        alpha = isMaximizingPlayer ? Math.max(alpha, bestScore) : alpha;
                        beta = isMaximizingPlayer ? beta : Math.min(beta, bestScore);
                        if (beta <= alpha) break; // Beta cut-off
                    }
                }
            }
        }
        return {score:bestScore};
    }

    public getMoves(){
        
    }

    // public makeMultipleCapture(board:Matrix<string>,move:IMove,currentPlayer:IPosition,player:Pion){
    //     let currentPos = currentPlayer;
    //     let newBoard = board.map((e)=>e.slice())
    //     const best = this.findBestCapture(newBoard,move.from, player);
    //     console.log("best",best);
    //     if (best.length > 1) {
    //         for (const path of best) {
    //             const move: IMove = {
    //                 to: path,
    //                 from: currentPos
    //             };
    //             const step_ = this.canCapture(newBoard, move, player);
    //             newBoard = this.makeMove(newBoard, move, step_, player);
    //             currentPos = path;
    //         }
    //     } else {
    //         // newBoard[move.from.x][move.from.y] = Pion.EMPTY;
    //         // newBoard[move.to.x][move.to.y] = player;
    //         const step_ = this.canCapture(newBoard,move,player);
    //         newBoard =this.makeMove(newBoard,move,step_,player);
    //     }
    //     return newBoard;
    // };
    public evaluate(board: Matrix<string>,depth:number): { score: number } {
        let score = 0;
        for (let row = 0; row < ROW; row++) {
            for (let col = 0; col < COLOUMN; col++) {
                const piece = board[row][col];
                if (piece == Pion.BLACK)
                    score += Math.round(1000/depth)
                if (piece == Pion.WHITE)
                    score -= Math.round(1000/depth)
            }
        }
        return { score: score };
    }
}

