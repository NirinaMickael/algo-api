import { Module } from '@nestjs/common';
import { AlgoService } from './algo.service';
import { AlgoController } from './algo.controller';
import { PlayerService } from './player';
import { TaquinService } from './taquin';

@Module({
  controllers: [AlgoController],
  providers: [AlgoService,PlayerService,TaquinService],
})
export class AlgoModule {}
