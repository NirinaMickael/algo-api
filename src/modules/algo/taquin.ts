import { Injectable } from "@nestjs/common";
import { IPosition, Matrix, QUEUE, Vector, result } from "src/@type/_type";
import { ACTION, CONST } from "src/Constante/constante";
@Injectable()
export class TaquinService{
    grid !: Vector<number>
    taille: number
    actions: IPosition[]
    finalState: Vector<number>;
    Cost: number = Infinity;
    constructor() {
        this.taille = 3;
        this.actions = ACTION;
        this.finalState = this.finalStateGrid();
        this.shuffle();
    }
    private isCanMove(positon: IPosition): boolean {
        if ((positon.x >= 0 && positon.x < this.taille) && (positon.y >= 0 && positon.y < this.taille)) {
            return true;
        } else {
            return false;
        }
    }
    public getNeighbors(state: Matrix<number>): Matrix<number>[] {
        let positionZero: IPosition = this.getPositionZero(state);
        let neighbors: Matrix<number>[] = [];
        for (const action of this.actions) {
            const nextPosition: IPosition = {
                x: positionZero.x + action.x,
                y: positionZero.y + action.y
            }
            if (this.isCanMove(nextPosition)) {
                const currentState: Matrix<number> = state.map(s => s.slice());
                const temp = currentState[nextPosition.x][nextPosition.y];
                currentState[nextPosition.x][nextPosition.y] = currentState[positionZero.x][positionZero.y];
                currentState[positionZero.x][positionZero.y] = temp;
                neighbors.push(currentState)
            }
        }
        return neighbors;
    }
    private getPositionZero(state: Matrix<number>): IPosition {
        for (let i = 0; i < this.taille; i++) {
            const j = state[i].indexOf(CONST.ZERO);
            if (state[i].indexOf(CONST.ZERO) != -1) {
                return {
                    x: i,
                    y: j
                }
            }
        }
        return { x: 0, y: 0 };
    };
    public BFS(grid:Vector<number>): Promise<result> {
        return new Promise((resolve,reject)=>{
            console.log(grid);
            const queue: QUEUE[] = [{
                vertice: grid, path: [grid]
            }];
            const visited = new Set<string>();
            visited.add(JSON.stringify(grid));
            let i = 0;
            let startTime = performance.now();
            while (queue.length) {
                const { vertice, path } = queue.shift() as QUEUE;
                if (JSON.stringify(vertice) == JSON.stringify(this.finalState)) {
                    const time = performance.now() - startTime;
                    return resolve({
                        bestPath: path,
                        time: time,
                        cost: path.length
                    });
                }
                const neighbors = this.getNeighbors(this.toMatrix(vertice, this.taille));
                for (const neighbor of neighbors) {
                    if (!visited.has(JSON.stringify(neighbor))) {
                        queue.push(
                            { vertice: neighbor.flat(), path: [...path, neighbor.flat()] }
                        )
                        visited.add(JSON.stringify(neighbor));
                    }
                }
            }
            return  resolve({
                bestPath: [],
                time: 0,
                cost: 0
            });
        })
        // 3,7,1,8,4,5,0,6,2
    }
    toMatrix(array: Vector<number>, n: number): Matrix<number> {
        const resultados: Matrix<number> = [];
        for (var i = 0; i < array.length; i += n) {
            resultados.push(array.slice(i, i + n))
        }
        return resultados;
    }
    public shuffle() {
        const array = this.finalState;
        const suffle_ = [...array].sort(() => Math.random() - 0.5);
        this.grid = suffle_;
    };

    private finalStateGrid(): Vector<number> {
        const initState: Vector<number> = []
        for (let i = 1; i < this.taille * this.taille; i++) {
            initState.push(i);
        }
        initState.push(0);
        return initState;
    }
    public countInversions(arr: Vector<number>) {
        let inversions = 0;
        for (let i = 0; i < arr.length - 1; i++) {
            for (let j = i + 1; j < arr.length; j++) {
                // if (arr[i] != 0 && arr[j] != 0) {
                if (arr[j] > arr[i]) {
                    inversions++;
                    // }
                }
            }
        }
        return inversions;
    }

}