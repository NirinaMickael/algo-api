import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { AlgoService } from './algo.service';
import { IData, Vector } from 'src/@type/_type';

@Controller('algo')
export class AlgoController {
  constructor(private readonly algoService: AlgoService) {}
  @Post("move-ai")
  MoveAI(@Body() data:IData){
    return this.algoService.MoveAI(data.board,data.pion);
  }
  
  @Post("taquin")
  TaquinAlgo(@Body() data:Vector<number>){
    return this.algoService.TaquinAI(data);
  }

}
