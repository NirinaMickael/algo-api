


export type Vector<T> = T[]
export type Matrix<T> = Vector<T>[]


export enum Type {
    ALGO,
    PLAYER
}

export enum Cage {
    EMPTY = "0",
    CROSS = "1",
    ZERO = "2"
}


export enum Pion {
    EMPTY = "0",
    BLACK = "1",
    WHITE = "2",
    ACTIVE = "3"
}



export interface IPosition {
    x: number;
    y: number;
}

export interface IMove {
    to: IPosition,
    from: IPosition
}
// export  class Position {
//     x : number
//     y : number
// }

export interface IBFS {
    noeud: IPosition, path: IPosition[]
}

export type IposCaptAndAsp={
    type:"capture" | "aspire",
    value :IPosition
};


export type INextMove={
    bestScore : number;
    bestMove : IMove
}
export interface IData{
    board:Matrix<string>;
    pion:Pion
}
export interface result {
    bestPath: Matrix<number>,
    time: number,
    cost: number
}

/// taquin
enum CONST {
    ZERO = 0
}

export type QUEUE = {
    vertice: Vector<number>,
    path: number[][]
}

