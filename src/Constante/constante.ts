import { IPosition, Matrix } from "../@type/_type";
export enum CONST {
    ZERO = 0
}
export const DIRECTION  :  IPosition[] = [
    {
        x:-1,
        y:1
    },
    {
        x:1,
        y:0
    },
    {
        x:-1,
        y:0
    },
    {
        x:0,
        y:1
    },
    {
        x:0,
        y:-1
    },
    {
        x:1,
        y:1
    },
    {
        x:1,
        y:-1
    },
    {
        x:-1,
        y:-1
    }
]

export const DIRECTIONKELY  :  IPosition[] = [
    {
        x:0,
        y:1
    },
    {
        x:0,
        y:-1
    },
    {
        x:-1,
        y:0
    },
    {
        x:1,
        y:0
    }
]

export const COLOUMN = 9
export const  ROW = 5


export const ACTION: IPosition[] = [
    { x: 1, y: 0 },
    { x: -1, y: 0 },
    { x: 0, y: 1 },
    { x: 0, y: -1 }
]
export const FINAL_STATE: Matrix<number>= [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 0]
]
export const CURRENT_STATE: Matrix<number> = [
    [8, 2, 3],
    [1, 0, 4],
    [7, 6, 5]
]