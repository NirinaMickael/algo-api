import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AlgoModule } from './modules/algo/algo.module';

@Module({
  imports: [AlgoModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
